Workshop réalisé avec les élèves du lycée Pasteur de Besançon.
-
BTS Communication visuelle 1ère année.

Travail graphique génératif imprimé en live sur ticket de caisses.

![](BTS_ThermalPrinting/pic/DSC08127.JPG)
=======

To do:

Contre les problèmes d'overflow, il faudrait essayer ceci:
https://learn.adafruit.com/mini-thermal-receipt-printer/hacking
ou voir si passer par
https://github.com/adafruit/Python-Thermal-Printer/blob/master/Adafruit_Thermal.py
permettrait de s'affranchir de ces soucis.
Cette solution parait idéale :
https://learn.adafruit.com/networked-thermal-printer-using-cups-and-raspberry-pi/overview

