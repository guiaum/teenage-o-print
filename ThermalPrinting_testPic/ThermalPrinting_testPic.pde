import gohai.simplereceiptprinter.*;
import processing.serial.*;
SimpleReceiptPrinter printer;
PImage loadedImg;
PImage displayedImage;
boolean doIDither=false;
int level = 127;

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    loadedImg = loadImage(selection.getAbsolutePath());
    imageToScreen(loadedImg);
  }
}

void imageToScreen(PImage img)
{
  if (img != null) {
    img.resize(printer.width, 0);
  } else {
    println("no image loaded"); // the file selected was not an image that could be loaded
  }
}

void setup() {
  size(384, 600);

  //CONNECTION TO THE PRINTER

  //If needed
  String[] ports = SimpleReceiptPrinter.list();
  println("Available serial ports:");
  printArray(ports);
  // you might need to use a different port
  String port = ports[3];
  println("Attempting to use " + port);

  printer = new SimpleReceiptPrinter(this, "/dev/tty.usbserial");

  //Select an image
  selectInput("Select a file to process:", "fileSelected");
}

/*
 * This implements Bill Atkinson's dithering algorithm
 */

void draw()
{
  background(255);

  if (loadedImg != null) 
  {
    dither();
    image(displayedImage, 0, 0);
  }
  
  println (level);
}

void keyPressed()
{
  if (key == 'd')doIDither = !doIDither;
  if (key == '+' && level<255 )level +=5;
  if (key == '-' && level>0 )level -=5;
  if (key == 'p' && level>0 )
  {
      noLoop();
       printer.printBitmap(get(0,0,displayedImage.width, displayedImage.height));
       printer.feed(3); 
       
  }
  }

  void dither() {
    if (doIDither == false)
    {
      displayedImage = loadedImg;
    } else {

      PImage tempImg = loadedImg.get();
      
      tempImg.loadPixels();

      for (int y=0; y < tempImg.height; y++) {
        for (int x=0; x < tempImg.width; x++) {
          // set current pixel and error
          float bright = brightness(tempImg.pixels[y*tempImg.width+x]);
          float err;
          if (bright <= level) {
            tempImg.pixels[y*tempImg.width+x] = 0x000000;
            err = bright;
          } else {
            tempImg.pixels[y*tempImg.width+x] = 0xffffff;
            err = bright-255;
          }
          // distribute error
          int offsets[][] = new int[][]{{1, 0}, {2, 0}, {-1, 1}, {0, 1}, {1, 1}, {0, 2}};
          for (int i=0; i < offsets.length; i++) {
            int x2 = x + offsets[i][0];
            int y2 = y + offsets[i][1];
            if (x2 < tempImg.width && y2 < tempImg.height) {
              float bright2 = brightness(tempImg.pixels[y2*tempImg.width+x2]);
              bright2 += err * 0.125;
              bright2 = constrain(bright2, 0.0, 255.0);
              tempImg.pixels[y2*tempImg.width+x2] = color(bright2);
            }
          }
        }
      }

      tempImg.updatePixels();
      displayedImage = tempImg;
    }
  }