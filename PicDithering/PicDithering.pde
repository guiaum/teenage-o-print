PImage loadedImg;
String nameOfImage;
int level = 127;

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    loadedImg = loadImage(selection.getAbsolutePath());
    nameOfImage = selection.getAbsolutePath();
    String[] splittedString = split(nameOfImage, '.');
    nameOfImage = splittedString[0];
    dither();
  }
}


void setup() {
  size(20, 20);
  //Select an image
  selectInput("Select a file to process:", "fileSelected");
}

/*
 * This implements Bill Atkinson's dithering algorithm
 */

void draw()
{
}


void dither() {
  PImage tempImg = loadedImg.get();

  tempImg.loadPixels();

  for (int y=0; y < tempImg.height; y++) {
    for (int x=0; x < tempImg.width; x++) {
      // set current pixel and error
      float bright = brightness(tempImg.pixels[y*tempImg.width+x]);
      float err;
      if (bright <= level) {
        tempImg.pixels[y*tempImg.width+x] = 0x000000;
        err = bright;
      } else {
        tempImg.pixels[y*tempImg.width+x] = 0xffffff;
        err = bright-255;
      }
      // distribute error
      int offsets[][] = new int[][]{{1, 0}, {2, 0}, {-1, 1}, {0, 1}, {1, 1}, {0, 2}};
      for (int i=0; i < offsets.length; i++) {
        int x2 = x + offsets[i][0];
        int y2 = y + offsets[i][1];
        if (x2 < tempImg.width && y2 < tempImg.height) {
          float bright2 = brightness(tempImg.pixels[y2*tempImg.width+x2]);
          bright2 += err * 0.125;
          bright2 = constrain(bright2, 0.0, 255.0);
          tempImg.pixels[y2*tempImg.width+x2] = color(bright2);
        }
      }
    }
  }

  tempImg.updatePixels();
  tempImg.save(nameOfImage+"_dithered.png");
  exit();
}