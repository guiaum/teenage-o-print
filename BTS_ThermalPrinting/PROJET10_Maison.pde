import java.util.Collections;


class PROJET10_Maison {
  StringList TITRE = new StringList();
  StringList TOITS = new StringList();
  StringList FENETRES = new StringList();
  StringList PORTES = new StringList();

  // Modifs possibles
  //int nbreLignesFenetres = 10;
  //si on veut que ce soit aléatoire: 
  int nbreLignesFenetres = (int)random(1,25);

  StringList FinalSelection = new StringList();
  PImage[] theImages;
  int heightImages;
  PGraphics pg;

  PROJET10_Maison()
  {
    println("PROJET10_Maison est créé.");  
    println();

    // On stocke le chemin de toutes les images dans des listes séparées
    loadPicsPaths(TITRE, "PROJ10_Maison/titre");
    loadPicsPaths(TOITS, "PROJ10_Maison/toits");
    loadPicsPaths(FENETRES, "PROJ10_Maison/fenetres");
    loadPicsPaths(PORTES, "PROJ10_Maison/portes");

    // Puis on génére les images
    generatePicNumbers();
    theImages = new PImage[FinalSelection.size()];

    // Quelles images ont été choisies?
    println();
    println("Les images d'intro suivantes ont été choisies:");
    for (int i=0; i<FinalSelection.size(); i++)
    {
      println( FinalSelection.get(i));
      // On les charge:
      theImages[i] = loadImage(FinalSelection.get(i));
    }

    // Calcul de la hauteur 
    // Titre
    heightImages +=  theImages[0].height;
    // Toits
    heightImages += theImages[1].height;
    //Fenetres
    heightImages += nbreLignesFenetres*theImages[5].height;
    // Portes
    heightImages += theImages[theImages.length-1].height;


    //Taille de l'image 
    println();
    println("Largeur : 384 px // Hauteur: "+ heightImages+" px");

    // OK! on connait les images, la hauteur totale, la largeur, on peut créer l'image entière !
    createPic();
  }

  private void createPic()
  {
    int indexY = 0;
    pg = createGraphics(384, heightImages);
    pg.beginDraw();
    pg.background(255);

    // Titre
    pg.image(theImages[0], 0, 0);
    indexY += theImages[0].height;

    // Toits
    for (int i = 1; i<4; i++)
    {
      pg.image(theImages[i], (i-1)*128, indexY);
    }
    indexY += theImages[1].height;
    
    // Fenetres
    // On boucle 
    for (int j =0; j<nbreLignesFenetres; j++)
    {
      for (int k = 0; k<3; k++)
      {
        pg.image(theImages[((j*3)+4)+k], (k*120)+12, indexY);
      }
      indexY+=theImages[4].height;
    }
    
    // Portes
    for (int l = 3; l>0; l--)
    {
      pg.image(theImages[theImages.length-l], ((l-1)*120)+12, indexY);
    }
    
    pg.endDraw();
  }

  public PGraphics getImage()
  {   
    return pg;
  }

  private void generatePicNumbers()
  {
    //Première partie
    FinalSelection.append(TITRE.get(0));

    // UNE LIGNE de toits
    for (int i = 0; i<3; i++)
    {
      FinalSelection.append(TOITS.get((int)random(TOITS.size())));
    }

    // PLUSIEURS LIGNES de fenêtres 
    for (int j = 0; j<nbreLignesFenetres; j++)
    {
      for (int k = 0; k<3; k++)
      {
        FinalSelection.append(FENETRES.get((int)random(FENETRES.size())));
      }
    }

    // UNE LIGNE de portes
    for (int l = 0; l<3; l++)
    {
      FinalSelection.append(PORTES.get((int)random(PORTES.size())));
    }
  }

  private void loadPicsPaths(StringList myStringList, String myDir)
  {
    println ("Je charge les images du dossier ../BTS_ThermalPrinting/data/"+ myDir);
    File dir = new File(sketchPath(""), "../BTS_ThermalPrinting/data/"+ myDir);
    if (dir.isDirectory()) {
      String[] contents = dir.list();
      for (int i = 0; i < contents.length; i++) {
        // skip hidden files and folders starting with a dot, load .png files only
        if (contents[i].charAt(0) == '.') continue;
        else if (contents[i].toLowerCase().endsWith(".png") || contents[i].toLowerCase().endsWith(".jpg") ) {
          File childFile = new File(dir, contents[i]);
          myStringList.append(childFile.getPath());
          println("---> "+contents[i]);
        }
      }
    }
    println();
  }
}