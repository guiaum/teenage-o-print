// Le programme choisit aléatoirement d'imprimer la version ou la version 2 du ticket,
// pour éviter de faire une image trop haute.
// L'algo de composition du ticket a été légèrement simplifié mais on est pas loin.
// Mon conseil: étalonner la taile des blancs entre les bulles.

class PROJET12_Lidl {

  StringList P1_1 = new StringList();
  StringList P1_2 = new StringList();
  StringList P1_3 = new StringList();
  StringList P1_4 = new StringList();
  StringList P2_1 = new StringList();
  StringList P2_2 = new StringList();
  StringList P2_3 = new StringList();

  StringList FinalSelection = new StringList();

  PImage[] theImages;
  int heightImages;

  PGraphics pg;


  PROJET12_Lidl()
  {
    println("PROJET12_Lidl est créé");  
    println();
    // On stocke le chemin de toutes les images dans des listes séparées
    loadPicsPaths(P1_1, "PROJ12_LIDL/1-Fichier1");
    loadPicsPaths(P1_2, "PROJ12_LIDL/1-Fichier2");
    loadPicsPaths(P1_3, "PROJ12_LIDL/1-Fichier3");
    loadPicsPaths(P1_4, "PROJ12_LIDL/1-Fichier4");
    loadPicsPaths(P2_1, "PROJ12_LIDL/2-1");
    loadPicsPaths(P2_2, "PROJ12_LIDL/2-2");
    loadPicsPaths(P2_3, "PROJ12_LIDL/2-3");

    // Puis on génére l'image en elle-même
    generatePicNumbers();

    theImages = new PImage[FinalSelection.size()];

    // Quelles images ont été choisies?
    println();
    println("Les images suivantes ont été choisies:");
    for (int i=0; i<FinalSelection.size(); i++)
    {
      println( FinalSelection.get(i));
      // On les charge:
      theImages[i] = loadImage(FinalSelection.get(i));
      heightImages += theImages[i].height;
    }

    //Taille de l'image
    println();
    println("Largeur : 384 px // Hauteur: "+ heightImages+" px");

    // OK! on connait les images, la hauteur totale, la largeur, on peut créer l'image entière !
    createPic();
  }

  private void createPic()
  {
    int indexY = 0;
    pg = createGraphics(384, heightImages);
    pg.beginDraw();
    pg.background(255);
    for (int i=0; i<theImages.length; i++)
    {
      pg.image(theImages[i], 0, indexY);
      indexY += theImages[i].height;
    }
    pg.endDraw();
  }

  public PGraphics getImage()
  {   
    return pg;
  }

  private void generatePicNumbers()
  {
    // Tirage aléatoire entre ticket ou 2
    if (random(0, 20)>10)
    {

      //Ticket 1
      FinalSelection.append(P1_1.get(0));

      // On mélange le dossier 3
      P1_3.shuffle();

      for (int j=0; j<P1_3.size(); j++)
      {
        if (random(0, 6)>3)
        {
          // Tirage au sort dossier 2 de 1 à 3 images max qui peuvent être identiques
          for (int i=0; i<(int)random(1, 3); i++)
          {
            FinalSelection.append(P1_2.get((int)random(P1_2.size())));
          }
        }
        FinalSelection.append(P1_3.get(j));
      }
      FinalSelection.append(P1_4.get(0));
      FinalSelection.append(P1_4.get(1));
    } else {

      //Ticket 2
      FinalSelection.append(P2_1.get(0));

      // On mélange le dossier 2
      P2_2.shuffle();
      for (int k=0; k<P1_3.size(); k++)
      {
        FinalSelection.append(P2_2.get(k));
      }

      FinalSelection.append(P2_3.get(0));
    }
  }

  private void loadPicsPaths(StringList myStringList, String myDir)
  {
    println ("Je charge les images du dossier ../BTS_ThermalPrinting/data/"+ myDir);
    File dir = new File(sketchPath(""), "../BTS_ThermalPrinting/data/"+ myDir);
    if (dir.isDirectory()) {
      String[] contents = dir.list();
      for (int i = 0; i < contents.length; i++) {
        // skip hidden files and folders starting with a dot, load .png files only
        if (contents[i].charAt(0) == '.') continue;
        else if (contents[i].toLowerCase().endsWith(".png")) {
          File childFile = new File(dir, contents[i]);
          myStringList.append(childFile.getPath());
          println("---> "+contents[i]);
        }
      }
    }
    println();
  }
}