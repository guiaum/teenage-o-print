import java.util.Calendar;
import gohai.simplereceiptprinter.*;
import processing.serial.*;

/////////////////////////////////
//IMPORTANT/////
// A passer sur "true" si l'imprimante est connectée
boolean printOnThermalPrinter = false;
//boolean printOnThermalPrinter = true;
/////////////////////////////////

SimpleReceiptPrinter printer;

int state = 0;
IntList myChoice = new IntList();
int myFinalChoice;

//Spécifiques à certains projets
int levelProj3;
int[] answers;

PGraphics myImage;
PImage myTicketToPrint;

PImage pic = new PImage();;

void setup() 
{
  size(800,800);
  //CONNECTION TO THE PRINTER
  if (printOnThermalPrinter)
  {
    //If needed
    String[] ports = SimpleReceiptPrinter.list();
    println("Available serial ports:");
    printArray(ports);
    // you might need to use a different port
    String port = ports[3];
    println("Attempting to use " + port);
    println();
    //C'est ici que l'on remplit le nom du port série de connection au printer
    //Choisir celui qui ressemble à tty.usbserial
    printer = new SimpleReceiptPrinter(this, "/dev/tty.usbserial");
  }
  pic = loadImage("../pic/DSC08127.JPG");
}


void draw()
{
  background(pic);
  switch (state)
  {
  case 0:
    // Choix du projet à imprimer
    printChoices();
    noLoop();
    break;

  case 1:
    // Choix du niveau de difficulté de Baldo
    printLevelBaldo();
    noLoop();
    break;

  case 2:
    // Questionnaire Totem
    printQuestionsTotem();
    noLoop();
    break;

  case 10:
    // C'est parti, on créé l'image
    switch (myFinalChoice)
    {
    case 0:
      PROJET1_Arbre myProject = new PROJET1_Arbre();
      myImage = myProject.getImage();
      state = 20;
      break;

    case 1:
      PROJET2_Abordage myProject2 = new PROJET2_Abordage();
      myImage = myProject2.getImage();
      state = 20;
      break;

    case 2:
      PROJET3_Baldo myProject3 = new PROJET3_Baldo(levelProj3);
      myImage = myProject3.getImage();
      state = 20;
      break;

    case 3:
      PROJET4_Trame myProject4 = new PROJET4_Trame();
      myImage = myProject4.getImage();
      state = 20;
      break;

    case 4:
      PROJET5_Macarena myProject5 = new PROJET5_Macarena();
      myImage = myProject5.getImage();
      state = 20;
      break;

    case 5:
      PROJET6_Deche myProject6 = new PROJET6_Deche();
      myImage = myProject6.getImage();
      state = 20;
      break;

    case 6:
      PROJET7_Faces myProject7 = new PROJET7_Faces();
      myImage = myProject7.getImage();
      state = 20;
      break;

    case 7:
      PROJET8_Totem myProject8 = new PROJET8_Totem(answers);
      myImage = myProject8.getImage();
      state = 20;
      break;

    case 8:
      PROJET9_Hiphop myProject9 = new PROJET9_Hiphop();
      myImage = myProject9.getImage();
      state = 20;
      break;

    case 9:
      PROJET10_Maison myProject10 = new PROJET10_Maison();
      myImage = myProject10.getImage();
      state = 20;
      break;

    case 10:
      PROJET11_Fishes myProject11 = new PROJET11_Fishes();
      myImage = myProject11.getImage();
      state = 20;
      break;

    case 11:
      PROJET12_Lidl myProject12 = new PROJET12_Lidl();
      myImage = myProject12.getImage();
      state = 20;
      break;
    }
    //noLoop();
    break;

    // On sauvegarde l'image pour la route  
  case 20:
    String myPicName = "Projet_"+(myFinalChoice+1)+"_"+timestamp()+".jpeg";
    myImage.save("xport/"+ myPicName);
    println();
    println ("Image sauvegardée sous le nom de: "+myPicName);
    delay(2000);
    // Puis on l'imprime 
    if (printOnThermalPrinter)
    {
      printer.printBitmap(myImage);
      printer.feed(3);
    }
    noLoop();


    break;
  }
}



void keyPressed()
{
  if ((int)key >= 48 && (int)key<= 57)
  {
    print ((int)key-48);
    myChoice.append((int)key-48);
  } else if (key == RETURN || key == ENTER) {
    println();
    println();
    if (state == 0) validateChoice();
    else if (state == 1) validateLevel();
    else if (state == 2) validateAnswers();
  }
}


String[] loadFirstLevelDir()
{
  File dir = new File(sketchPath(""), "../BTS_ThermalPrinting/data/");
  String[] contents = new String[dir.list().length];
  if (dir.isDirectory()) {
    contents = dir.list();
  }
  return contents;
}

void printChoices()
{
  printArray(loadFirstLevelDir());
  println();
  println("Tape le numéro du projet + return");
}

void validateChoice()
{
  delay(500);
  if (myChoice.size()==0)
  {
    println("Tu as oublié de taper un nombre ! Essaie encore.");
  } else if (myChoice.size()>2) {
    println("Hmmm, c'est embarrassant, tu dois choisir entre 0 et 11…");
  } else if (myChoice.size() == 2) {
    myFinalChoice = (myChoice.get(0)*10) + (myChoice.get(1));
    if (myFinalChoice > 11)println("Hmmm, c'est embarrassant, tu dois choisir entre 0 et 11…");
    else {
      println("Tu as choisi le numéro "+myFinalChoice+". Tu as raison, c'est le meilleur projet!");
      println();
      // Entrées supplémentaires nécessaires pour certains projets
      checkProj();
    }
  } else if (myChoice.size() == 1) {
    myFinalChoice = myChoice.get(0);
    println("Tu as choisi le numéro "+myFinalChoice+". Tu as raison, c'est le meilleur projet!");
    println();
    // Entrées supplémentaires nécessaires pour certains projets
    checkProj();
  }
  myChoice.clear();
}

// Validation niveau Baldo
void validateLevel()
{
  if (myChoice.size()==0)
  {
    println("Tu as oublié de taper un nombre ! Essaie encore.");
  } else if (myChoice.size()>1 || myChoice.get(0)>4) {
    println("Hmmm, c'est embarrassant, tu dois choisir entre 1 et 4…");
  } else if (myChoice.size() == 1) {
    levelProj3 = myChoice.get(0);
    println("Tu as choisi le niveau "+levelProj3+". C'est assez courageux.");
    println();
    state = 10;
    loop();
  }
  myChoice.clear();
}

// Validation réponses totem
void validateAnswers()
{
  if (myChoice.size()<7)
  {
    println("Il manque des réponses, on recommence tout!");
    myChoice.clear();
  } else  if (myChoice.size()>7)
  {
    println("Il y a trop de de réponses, c'est louche, on recommence tout!");
    myChoice.clear();
  } else if (myChoice.size()==7) {
    boolean isOnly1or0 = true;
    for (int i = 0; i<7; i++)
    {
      if  (myChoice.get(i) > 1)
      {
        isOnly1or0 = false;
      }
    }
    if (isOnly1or0 == false)
    {
      println("Tu as répondu autre chose que 0 ou 1, on recommence tout!");
      myChoice.clear();
    } else {
      answers = myChoice.array();  
      state = 10;
      loop();
    }
    myChoice.clear();
  }
}

void checkProj()
{
  if (myFinalChoice == 2)
  {
    state = 1;
  } else if (myFinalChoice == 7)
  {
    state = 2;
  } else  
  {
    state = 10;
  }
  loop();
}

void printLevelBaldo()
{
  delay(500);
  println("Choisis un niveau de difficuté, de 1 à 4 (1 est le plus facile):");
  delay(500);
}

void printQuestionsTotem()
{
  delay(500);
  println("Réponds aux question suivantes: (1) OUI    (0) NON");
  println("L'examinateur doit accumuler les 0 et les 1 sur une seule ligne et presser RETURN.");
  println("--");
  delay(100);
  println("1-Tu préfères une bonne petite soirée tout seul qu'une grosse soirée en boîte?");
  delay(100);
  println("2-Tu es plutôt du genre à engager les conversations dans un groupe?");
  delay(100);
  println("3-Tu préfères dire la vérité cash quitte à blesser les gens?");
  delay(100);
  println("4-Tu préfères un voyage précisément organisé à un départ sur un coup de tête?");
  delay(100);
  println("5-Tu es plus balade shopping que balade en forêt?");
  delay(100);
  println("6-Tu préfères éviter de rendre des services aux autres pour ne pas qu'ils te prennent pour la boniche.");
  delay(100);
  println("7-Tu aimes impressionner les autres.");
  delay(500);
}

String timestamp() {
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", Calendar.getInstance());
}