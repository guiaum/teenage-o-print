class PROJET5_Macarena {
  StringList DEBUT = new StringList();
  StringList PICS = new StringList();

  // Possibilité de modif
  int nberOfPics = 5;

  StringList FinalSelection = new StringList();

  PImage[] theImages;
  int heightImages;

  PGraphics pg;


  PROJET5_Macarena()
  {
    println("PROJET5_Macarena est créé");  
    println();
    // On stocke le chemin de toutes les images dans deux listes
    loadPicsPaths(DEBUT, "PROJ05_Macarena/DEBUT");
    loadPicsPaths(PICS, "PROJ05_Macarena/PICS");

    // Puis on génére l'image en elle-même
    generatePicNumbers();

    theImages = new PImage[FinalSelection.size()];

    // Quelles images ont été choisies?
    println();
    println("Les images suivantes ont été choisies:");
    for (int i=0; i<FinalSelection.size(); i++)
    {
      println( FinalSelection.get(i));
      // On les charge:
      theImages[i] = loadImage(FinalSelection.get(i));
      heightImages += theImages[i].height;
    }

    //Taille de l'image
    println();
    println("Largeur : 384 px // Hauteur: "+ heightImages+" px");

    // OK! on connait les images, la hauteur totale, la largeur, on peut créer l'image entière !
    createPic();
  }

  private void createPic()
  {
    int indexY = 0;
    pg = createGraphics(384, heightImages);
    pg.beginDraw();
    pg.background(255);
    for (int i=0; i<theImages.length; i++)
    {
      pg.image(theImages[i], 0, indexY);
      indexY += theImages[i].height;
    }
    pg.endDraw();
  }

  public PGraphics getImage()
  {   
    return pg;
  }

  private void generatePicNumbers()
  {
    //Première partie
    FinalSelection.append(DEBUT.get((int)random(DEBUT.size())));

    //Deuxième partie
    int actualNberOfPics = 0;
    int prevPic = 0;
    while (actualNberOfPics<nberOfPics)
    {
      int whatPic = (int)random(PICS.size());
      if (actualNberOfPics == 0)
      {
        FinalSelection.append(PICS.get(whatPic));
        prevPic = whatPic;
        actualNberOfPics++;
      } else {
        if (whatPic != prevPic)
        {
          FinalSelection.append(PICS.get(whatPic));
          prevPic = whatPic;
          actualNberOfPics++;
        }
      }
    }
  }

  private void loadPicsPaths(StringList myStringList, String myDir)
  {
    println ("Je charge les images du dossier ../BTS_ThermalPrinting/data/"+ myDir);
    File dir = new File(sketchPath(""), "../BTS_ThermalPrinting/data/"+ myDir);
    if (dir.isDirectory()) {
      String[] contents = dir.list();
      for (int i = 0; i < contents.length; i++) {
        // skip hidden files and folders starting with a dot, load .png files only
        if (contents[i].charAt(0) == '.') continue;
        else if (contents[i].toLowerCase().endsWith(".png")) {
          File childFile = new File(dir, contents[i]);
          myStringList.append(childFile.getPath());
          println("---> "+contents[i]);
        }
      }
    }
    println();
  }
}