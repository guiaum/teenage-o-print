class PROJET7_Faces {

  StringList FIN = new StringList();
  StringList DEBUT = new StringList();
  StringList FACES = new StringList();

  StringList FinalSelection = new StringList();
  
  PImage[] theImages;
  int heightImages;
  
  PGraphics pg;
  
  // Possibilité de modif
  int limiteTirage = 7;

  PROJET7_Faces()
  {
    println("PROJET7_Faces est créé");  
    println();
    // On stocke le chemin de toutes les images dans des listes séparées
    loadPicsPaths(FIN, "PROJ07_Faces/FIN");
    loadPicsPaths(DEBUT, "PROJ07_Faces/DEBUT");
    loadPicsPaths(FACES, "PROJ07_Faces/FACES");
    
    // Puis on génére l'image en elle-même
    generatePicNumbers();
    
    theImages = new PImage[FinalSelection.size()];
    
    // Quelles images ont été choisies?
    println();
    println("Les images suivantes ont été choisies:");
    for (int i=0; i<FinalSelection.size(); i++)
    {
     println( FinalSelection.get(i));
     // On les charge:
     theImages[i] = loadImage(FinalSelection.get(i));
     heightImages += theImages[i].height;
    }
    
    //Taille de l'image
    println();
    println("Largeur : 384 px // Hauteur: "+ heightImages+" px");
    
    // OK! on connait les images, la hauteur totale, la largeur, on peut créer l'image entière !
    createPic();
  }
  
  private void createPic()
  {
    int indexY = 0;
    pg = createGraphics(384, heightImages);
    pg.beginDraw();
    pg.background(255);
    for (int i=0; i<theImages.length; i++) 
    {
      pg.image(theImages[i], 0,indexY);
      indexY += theImages[i].height;
    }
    pg.endDraw();
  }
  
  public PGraphics getImage()
  {   
    return pg;
  }
  
  private void generatePicNumbers()
  {
      //Première partie
      FinalSelection.append(FIN.get(0));
      //Deuxième partie
      while((int)random(0,limiteTirage)<limiteTirage-1)
      {
       FinalSelection.append(FACES.get((int)random(FACES.size()))); 
      }
      //Troisième partie
      FinalSelection.append(DEBUT.get((int)random(DEBUT.size())));
    
  }
  
  private void loadPicsPaths(StringList myStringList, String myDir)
  {
    println ("Je charge les images du dossier ../BTS_ThermalPrinting/data/"+ myDir);
    File dir = new File(sketchPath(""), "../BTS_ThermalPrinting/data/"+ myDir);
    if (dir.isDirectory()) {
      String[] contents = dir.list();
      for (int i = 0; i < contents.length; i++) {
        // skip hidden files and folders starting with a dot, load .png files only
        if (contents[i].charAt(0) == '.') continue;
        else if (contents[i].toLowerCase().endsWith(".png")) {
          File childFile = new File(dir, contents[i]);
          myStringList.append(childFile.getPath());
          println("---> "+contents[i]);
        }
      }
    }
    println();
  }
}