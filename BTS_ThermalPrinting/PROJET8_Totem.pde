import java.util.Collections;


class PROJET8_Totem {
  StringList PICS = new StringList();
  StringList TITRE = new StringList();

  int[] answers;

  StringList FinalSelection = new StringList();
  PImage[] theImages;
  int heightImages;
  PGraphics pg;


  PROJET8_Totem(int[] pAnswers)
  {
    answers = pAnswers;
    println("PROJET8_Totem est créé");  
    println();


    // On stocke le chemin de toutes les images dans des listes séparées
    loadPicsPaths(PICS, "PROJ08_Totem/PICS");
    loadPicsPaths(TITRE, "PROJ08_Totem/TITRE");


    // Puis on génére les différents chemins d'images en fonction des réponses
    generatePicNumbers();
    theImages = new PImage[FinalSelection.size()];


    // Quelles images ont été choisies?
    println();
    println("Les images suivantes ont été choisies:");
    for (int i=0; i<FinalSelection.size(); i++)
    {
      println( FinalSelection.get(i));
      // On les charge:
      theImages[i] = loadImage(FinalSelection.get(i));
      heightImages += theImages[i].height;
    }

    //Taille de l'image 
    println();
    println("Largeur : 384 px // Hauteur: "+ heightImages+" px");

    // OK! on connait les images, la hauteur totale, la largeur, on peut créer l'image entière !
    createPic();
  }

  private void createPic()
  {
    int indexY = 0;
    pg = createGraphics(384, heightImages);
    pg.beginDraw();
    pg.background(255);
    
    for (int i=0; i<theImages.length; i++)
    {
      pg.image(theImages[i], 0, indexY);
      indexY += theImages[i].height;
    }


    pg.endDraw();
  }

  public PGraphics getImage()
  {   
    return pg;
  }




  private void generatePicNumbers()
  {
    
    
    StringList TempSelection = new StringList();
    
    // On sélectionne chaque image en fonction des réponses
    for (int i = 1; i<8; i++)
    {
      if (answers[i-1] == 1)
      {
        TempSelection.append(PICS.get( (i*2)-2) );
      } else {
        TempSelection.append(PICS.get((i*2)-1));
      }
    }
    
    ///////
    //Titre
    FinalSelection.append(TITRE.get(0));
    //Première image
    FinalSelection.append(TempSelection.get(0));

    // On sauvegarde dans un coin la dernière image
    String lastPic = TempSelection.get(TempSelection.size()-1);
    
    // On enlève la première et la dernière de la liste temporaire
    TempSelection.remove(0);
    TempSelection.remove(TempSelection.size()-1);
    // On mélange !
    TempSelection.shuffle();
    
    //On copie de temp vers final
    for (int j = 0; j<TempSelection.size(); j++)
    {
      FinalSelection.append(TempSelection.get(j));
    }
    
    // On remet la dernière   
    FinalSelection.append(lastPic);
 
  }

  private void loadPicsPaths(StringList myStringList, String myDir)
  {
    println ("Je charge les images du dossier ../BTS_ThermalPrinting/data/"+ myDir);
    File dir = new File(sketchPath(""), "../BTS_ThermalPrinting/data/"+ myDir);
    if (dir.isDirectory()) {
      String[] contents = dir.list();
      for (int i = 0; i < contents.length; i++) {
        // skip hidden files and folders starting with a dot, load .png files only
        if (contents[i].charAt(0) == '.') continue;
        else if (contents[i].toLowerCase().endsWith(".png") || contents[i].toLowerCase().endsWith(".jpg") ) {
          File childFile = new File(dir, contents[i]);
          myStringList.append(childFile.getPath());
          println("---> "+contents[i]);
        }
      }
    }
    println();
  }
}