import java.util.Collections;


class PROJET6_Deche {
  StringList BARRE = new StringList();
  StringList FLAMMES = new StringList();
  StringList GAINS = new StringList();
  StringList PERDU = new StringList();
  StringList TITRE = new StringList();
  StringList VISUELS = new StringList();

  int biggestPic;



  StringList FinalSelection = new StringList();
  PImage[] theImages;
  int heightImages;
  PGraphics pg;


  PROJET6_Deche()
  {
    println("PROJET6_Deche est créé.");  
    println();

    // On stocke le chemin de toutes les images dans des listes séparées
    loadPicsPaths(BARRE, "PROJ06_CestLaDeche/barre");
    loadPicsPaths(FLAMMES, "PROJ06_CestLaDeche/flammesfindejeu");
    loadPicsPaths(GAINS, "PROJ06_CestLaDeche/gains");
    loadPicsPaths(PERDU, "PROJ06_CestLaDeche/perdu");
    loadPicsPaths(TITRE, "PROJ06_CestLaDeche/titre");
    loadPicsPaths(VISUELS, "PROJ06_CestLaDeche/visuels");


    // Puis on génére les images (le tirage se passe là aussi)
    generatePicNumbers();
    theImages = new PImage[FinalSelection.size()];

    // Quelles images ont été choisies?
    println();
    println("Les images d'intro suivantes ont été choisies:");
    for (int i=0; i<FinalSelection.size(); i++)
    {
      println( FinalSelection.get(i));
      // On les charge:
      theImages[i] = loadImage(FinalSelection.get(i));
    }
    
    // Calcul de la hauteur 
    // Titre
    heightImages +=  theImages[0].height;
    // Visuels x 1
    biggestPic = max(theImages[1].height, theImages[2].height, theImages[3].height);

    heightImages +=  biggestPic;
    // Barre
    heightImages +=  theImages[4].height;
    // Gain
    heightImages +=  theImages[5].height;
    // Flammes finales
    heightImages +=  theImages[6].height;
    
    //Taille de l'image 
    println();
    println("Largeur : 384 px // Hauteur: "+ heightImages+" px");

    // OK! on connait les images, la hauteur totale, la largeur, on peut créer l'image entière !
    createPic();
  }

  private void createPic()
  {
    int indexY = 0;
    pg = createGraphics(384, heightImages);
    pg.beginDraw();
    pg.background(255);
    
    // Titre
    pg.image(theImages[0], 0,0);
    indexY += theImages[0].height;
    
    // Visuels
    
    pg.image(theImages[1], 42,indexY + (biggestPic-theImages[1].height)/2);
    pg.image(theImages[2], 142,indexY+ (biggestPic-theImages[2].height)/2);
    pg.image(theImages[3], 242,indexY+ (biggestPic-theImages[3].height)/2);
    
    indexY += biggestPic;
    
    // Barre
    pg.image(theImages[4], 0,indexY);
    indexY += theImages[4].height;
    
    // Gain
    pg.image(theImages[5], (384-theImages[5].width)/2,indexY);
    indexY += theImages[5].height;
    
    // Flammes de la fin
    pg.image(theImages[6], 0,indexY);

    pg.endDraw();
  }

  public PGraphics getImage()
  {   
    return pg;
  }

  private void generatePicNumbers()
  {
    //Première partie
    FinalSelection.append(TITRE.get(0));

    //Deuxième partie, tirage de 3 chiffres dans un tableau
    //DEBUG
    //int[] myGame = {5, 5, 5};
    int[] myGame = {(int)random(0, 7), (int)random(0, 7), (int)random(0, 7)};
    FinalSelection.append(VISUELS.get(myGame[0]));
    FinalSelection.append(VISUELS.get(myGame[1]));
    FinalSelection.append(VISUELS.get(myGame[2]));
    // Barre
    FinalSelection.append(BARRE.get(0));
    // On checke le résultat pour savoir ce que l'on va afficher ensuite
    //Inégalité
    if (myGame[0] != myGame[1] || myGame[0] != myGame[2]) // YOU LOOSE
    {
      FinalSelection.append(PERDU.get(0));
    } 
    //Ou égalité
    else if (myGame[0] == myGame[1] && myGame[0] == myGame[2]) // YOU WIN
    {
      // on affiche le gain dont l'index est identique au visuel
      FinalSelection.append(GAINS.get(myGame[0]));
    }
    // Flammes Finales
    FinalSelection.append(FLAMMES.get(0));
  }






  private void loadPicsPaths(StringList myStringList, String myDir)
  {
    println ("Je charge les images du dossier ../BTS_ThermalPrinting/data/"+ myDir);
    File dir = new File(sketchPath(""), "../BTS_ThermalPrinting/data/"+ myDir);
    if (dir.isDirectory()) {
      String[] contents = dir.list();
      for (int i = 0; i < contents.length; i++) {
        // skip hidden files and folders starting with a dot, load .png files only
        if (contents[i].charAt(0) == '.') continue;
        else if (contents[i].toLowerCase().endsWith(".png") || contents[i].toLowerCase().endsWith(".jpg") ) {
          File childFile = new File(dir, contents[i]);
          myStringList.append(childFile.getPath());
          println("---> "+contents[i]);
        }
      }
    }
    println();
  }
}