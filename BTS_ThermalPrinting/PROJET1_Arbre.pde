// 1 image hors format a été supprimée

class PROJET1_Arbre {

  StringList CIME = new StringList();
  StringList MILIEUX = new StringList();
  StringList TRONC = new StringList();
  StringList SOUCHE = new StringList();

  StringList FinalSelection = new StringList();
  
  PImage[] theImages;
  int heightImages;
  
  PGraphics pg;
  
  // Possibilité de modif
  int limiteTirage = 7;

  PROJET1_Arbre()
  {
    println("PROJET1_Arbre est créé");  
    println();
    // On stocke le chemin de toutes les images dans des listes séparées
    loadPicsPaths(CIME, "PROJ01_ArbreSansFin/1_CIME");
    loadPicsPaths(MILIEUX, "PROJ01_ArbreSansFin/2_MILIEUX");
    loadPicsPaths(TRONC, "PROJ01_ArbreSansFin/3_TRONC");
    loadPicsPaths(SOUCHE, "PROJ01_ArbreSansFin/4_SOUCHE");
    
    // Puis on génére l'image en elle-même
    generatePicNumbers();
    
    theImages = new PImage[FinalSelection.size()];
    
    // Quelles images ont été choisies?
    println();
    println("Les images suivantes ont été choisies:");
    for (int i=0; i<FinalSelection.size(); i++)
    {
     println( FinalSelection.get(i));
     // On les charge:
     theImages[i] = loadImage(FinalSelection.get(i));
     heightImages += theImages[i].height;
    }
    
    //Taille de l'image
    println();
    println("Largeur : 384 px // Hauteur: "+ heightImages+" px");
    
    // OK! on connait les images, la hauteur totale, la largeur, on peut créer l'image entière !
    createPic();
  }
  
  private void createPic()
  {
    int indexY = 0;
    pg = createGraphics(384, heightImages);
    pg.beginDraw();
    pg.background(255);
    for (int i=0; i<theImages.length; i++)
    {
      pg.image(theImages[i], 0,indexY);
      indexY += theImages[i].height;
    }
    pg.endDraw();
  }
  
  public PGraphics getImage()
  {   
    return pg;
  }
  
  private void generatePicNumbers()
  {
    if (random(0,30)<2){
      println("Juste une souche !");
      FinalSelection.append(SOUCHE.get((int)random(SOUCHE.size())));
    }
    else{
      //Première partie
      FinalSelection.append(TRONC.get((int)random(TRONC.size())));
      //Deuxième partie
      while((int)random(0,limiteTirage)<limiteTirage-1)
      {
       FinalSelection.append(MILIEUX.get((int)random(MILIEUX.size()))); 
      }
      //Troisième partie
      FinalSelection.append(CIME.get((int)random(CIME.size())));
    }
  }
  
  private void loadPicsPaths(StringList myStringList, String myDir)
  {
    println ("Je charge les images du dossier ../BTS_ThermalPrinting/data/"+ myDir);
    File dir = new File(sketchPath(""), "../BTS_ThermalPrinting/data/"+ myDir);
    if (dir.isDirectory()) {
      String[] contents = dir.list();
      for (int i = 0; i < contents.length; i++) {
        // skip hidden files and folders starting with a dot, load .png files only
        if (contents[i].charAt(0) == '.') continue;
        else if (contents[i].toLowerCase().endsWith(".png")) {
          File childFile = new File(dir, contents[i]);
          myStringList.append(childFile.getPath());
          println("---> "+contents[i]);
        }
      }
    }
    println();
  }
}