import java.util.Collections;

// Voir redimensionnement des fichiers intro

class PROJET3_Baldo {
  StringList INTRO = new StringList();
  StringList LEVEL = new StringList();
  StringList CHAPEAUX = new StringList();
  StringList TETES = new StringList();
  StringList BUSTES = new StringList();
  StringList JAMBES = new StringList();

  int level;
  int maxPersos;
  int minLignes = 4;
  int lineHeight = 72;
  int lineSpace = 18;

  //Persos
  ArrayList <int[]> myPersos = new ArrayList();
  PImage[] theHats;
  PImage[] theHeads;
  PImage[] theChests;
  PImage[] theLegs;



  StringList FinalSelection = new StringList();
  PImage[] theImages;
  int heightImages;
  PGraphics pg;


  PROJET3_Baldo(int pLevel)
  {
    level = pLevel;
    println("PROJET3_Baldo est créé avec le niveau "+level);  
    println();
    maxPersos = level*minLignes*5;

    // On stocke le chemin de toutes les images dans des listes séparées
    loadPicsPaths(INTRO, "PROJ03_Baldo/INTRO");
    loadPicsPaths(LEVEL, "PROJ03_Baldo/LEVEL");
    loadPicsPaths(CHAPEAUX, "PROJ03_Baldo/CHAPEAUX");
    loadPicsPaths(TETES, "PROJ03_Baldo/TETES");
    loadPicsPaths(BUSTES, "PROJ03_Baldo/BUSTES");
    loadPicsPaths(JAMBES, "PROJ03_Baldo/JAMBES");

    // On prépare les images, on aura besoin de tout le monde pour les persos
    theHats = new PImage[CHAPEAUX.size()];
    for (int i=0; i<CHAPEAUX.size(); i++)
    {
      theHats[i] = loadImage(CHAPEAUX.get(i));
    }

    theHeads = new PImage[TETES.size()];
    for (int i=0; i<TETES.size(); i++)
    {
      theHeads[i] = loadImage(TETES.get(i));
    }

    theChests = new PImage[BUSTES.size()];
    for (int i=0; i<BUSTES.size(); i++)
    {
      theChests[i] = loadImage(BUSTES.get(i));
    }

    theLegs = new PImage[JAMBES.size()];
    for (int i=0; i<JAMBES.size(); i++)
    {
      theLegs[i] = loadImage(JAMBES.get(i));
    }

    // Puis on génére l'intro
    generatePicNumbers();
    theImages = new PImage[FinalSelection.size()];

    // Ensuite les persos
    generatePersos();


    // Quelles images ont été choisies?
    println();
    println("Les images d'intro suivantes ont été choisies:");
    for (int i=0; i<FinalSelection.size(); i++)
    {
      println( FinalSelection.get(i));
      // On les charge:
      theImages[i] = loadImage(FinalSelection.get(i));
      heightImages += theImages[i].height;
    }
    // Interligne 
    heightImages += lineSpace;

    // Calcul de la hauteur suivant le nombre de ligne
    heightImages += (maxPersos/5)*(lineHeight+ lineSpace) ;

    //Taille de l'image 
    println();
    println("Largeur : 384 px // Hauteur: "+ heightImages+" px");

    // OK! on connait les images, la hauteur totale, la largeur, on peut créer l'image entière !
    createPic();
  }

  private void createPic()
  {
    int indexY = 0;
    pg = createGraphics(384, heightImages);
    pg.beginDraw();
    pg.background(255);
    // Intro
    for (int i=0; i<theImages.length; i++)
    {
      int offsetX = (384-theImages[i].width)/2;
      pg.image(theImages[i], offsetX , indexY);
      indexY += theImages[i].height;
    }
    indexY += lineSpace;
    // Persos
    for (int row=0; row<(maxPersos/5); row++)
    {
      for (int col=0; col<5; col++)
      {
       int index = row*5 + col;
       int theX = (col*66)+27;
       int theY = indexY+(row*(72+lineSpace));
       int[] myArray = myPersos.get(index);

       //Chapeau
       pg.image(theHats[myArray[0]], theX, theY);
       //Tete
       pg.image(theHeads[myArray[1]], theX, theY+18);
       //Buste
       pg.image(theChests[myArray[2]], theX, theY+36);
       //Jambes
       pg.image(theLegs[myArray[3]], theX, theY+54);
       
       //DEBUG
       /*
       stroke(1);
       noFill();
       pg.rect((col*66)+27, indexY+(row*(72+lineSpace)), 66 , 72);
       */
       
      }
    }
    
    
    pg.endDraw();
  }

  public PGraphics getImage()
  {   
    return pg;
  }

  private void generatePersos()
  {
    int createdPersos = 0;
    while (createdPersos<maxPersos-1)
    {
      int myHat = (int)random(CHAPEAUX.size());
      int myHead = (int)random(TETES.size());
      int myChest = (int)random(BUSTES.size());
      int myLegs = (int)random(JAMBES.size());

      // On vérifie que l'on a pas tiré Baldo par hasard !
      if (myHat != 0 && myHead != 0 && myChest != 0 && myLegs != 0)
      {
        int[] myArray = {myHat, myHead, myChest, myLegs};
        //DEBUG
        /*
        println("Perso n°"+createdPersos+" ");
        printArray(myArray);
        println();
        */
        myPersos.add(myArray);
        createdPersos++;
      }
    }
    // On ajoute Baldo
    int[] myBaldo = {0, 0, 0, 0};
    myPersos.add(myBaldo);

    //On secoue tout ça pour mélanger
    Collections.shuffle(myPersos);
  }


  private void generatePicNumbers()
  {
    //Première partie
    FinalSelection.append(INTRO.get((int)random(INTRO.size())));

    //Deuxième partie
    FinalSelection.append(LEVEL.get(level-1));
  }

  private void loadPicsPaths(StringList myStringList, String myDir)
  {
    println ("Je charge les images du dossier ../BTS_ThermalPrinting/data/"+ myDir);
    File dir = new File(sketchPath(""), "../BTS_ThermalPrinting/data/"+ myDir);
    if (dir.isDirectory()) {
      String[] contents = dir.list();
      for (int i = 0; i < contents.length; i++) {
        // skip hidden files and folders starting with a dot, load .png files only
        if (contents[i].charAt(0) == '.') continue;
        else if (contents[i].toLowerCase().endsWith(".png") || contents[i].toLowerCase().endsWith(".jpg") ) {
          File childFile = new File(dir, contents[i]);
          myStringList.append(childFile.getPath());
          println("---> "+contents[i]);
        }
      }
    }
    println();
  }
}