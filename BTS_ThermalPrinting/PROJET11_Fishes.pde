class PROJET11_Fishes {

  StringList TITRE = new StringList();
  StringList TETES = new StringList();
  StringList CORPS = new StringList();
  StringList QUEUES = new StringList();
  StringList FOND = new StringList();

  StringList FinalSelection = new StringList();

  //Modif possible
  int limiteTirage = 5;
  int limNbreDecors = 4;
  // Le moyen de nettoyer les déchets
  boolean fond = true;
  //boolean fond = false;
  
  PImage[] theImages;
  int heightImages;
  PGraphics pg;


  PROJET11_Fishes()
  {
    println("PROJET11_Fishes est créé");  
    println();

    // On stocke le chemin de toutes les images dans des listes séparées
    loadPicsPaths(TITRE, "PROJ11_Fishes/Titre");
    loadPicsPaths(TETES, "PROJ11_Fishes/Tetes");
    loadPicsPaths(CORPS, "PROJ11_Fishes/Corps");
    loadPicsPaths(QUEUES, "PROJ11_Fishes/Queues");
    loadPicsPaths(FOND, "PROJ11_Fishes/Fond");

    // Puis on génére l'image en elle-même
    generatePicNumbers();

    theImages = new PImage[FinalSelection.size()];

    // Quelles images ont été choisies?
    println();
    println("Les images suivantes ont été choisies:");
    for (int i=0; i<FinalSelection.size(); i++)
    {
      println( FinalSelection.get(i));
      // On les charge:
      theImages[i] = loadImage(FinalSelection.get(i));
      heightImages += theImages[i].height;
    }

    //Taille de l'image
    println();
    println("Largeur : 384 px // Hauteur: "+ heightImages+" px");

    // OK! on connait les images, la hauteur totale, la largeur, on peut créer l'image entière !
    createPic();
  }

  private void createPic()
  {
    int indexY = 0;
    pg = createGraphics(384, heightImages);
    pg.beginDraw();
    pg.background(255);
    for (int i=0; i<theImages.length; i++)
    {
      pg.image(theImages[i], 0, indexY);
      indexY += theImages[i].height;
    }

    if (fond)
    {
      pg.blendMode(MULTIPLY);
      // On ajoute un peu de décor
      for (int i =0; i<limNbreDecors; i++);
      {
        PImage monFond = loadImage(FOND.get((int)random(FOND.size())));
        pg.image(monFond, random(0, 384), random(0, heightImages));
      }
    }

    pg.endDraw();
  }

  public PGraphics getImage()
  {   
    return pg;
  }

  private void generatePicNumbers()
  {
    // Titre
    FinalSelection.append(TITRE.get(0));

    // Tete
    FinalSelection.append(TETES.get((int)random(TETES.size())));

    //Corps
    while ((int)random(0, limiteTirage)<limiteTirage-1)
    {
      FinalSelection.append(CORPS.get((int)random(CORPS.size())));
    }

    //Queue
    FinalSelection.append(QUEUES.get((int)random(QUEUES.size())));
  }

  private void loadPicsPaths(StringList myStringList, String myDir)
  {
    println ("Je charge les images du dossier ../BTS_ThermalPrinting/data/"+ myDir);
    File dir = new File(sketchPath(""), "../BTS_ThermalPrinting/data/"+ myDir);
    if (dir.isDirectory()) {
      String[] contents = dir.list();
      for (int i = 0; i < contents.length; i++) {
        // skip hidden files and folders starting with a dot, load .png files only
        if (contents[i].charAt(0) == '.') continue;
        else if (contents[i].toLowerCase().endsWith(".png")) {
          File childFile = new File(dir, contents[i]);
          myStringList.append(childFile.getPath());
          println("---> "+contents[i]);
        }
      }
    }
    println();
  }
}