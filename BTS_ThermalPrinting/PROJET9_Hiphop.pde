import java.util.Collections;


class PROJET9_Hiphop {
  StringList TITRE = new StringList();
  StringList G1 = new StringList();
  StringList G2 = new StringList();
  StringList G3 = new StringList();
  
  // Modifs possibles
  int nbreLignes = 16;
  int blank = 80; //en pixels
  
  StringList FinalSelection = new StringList();
  PImage[] theImages;
  int heightImages;
  PGraphics pg;

  PROJET9_Hiphop()
  {
    println("PROJET9_Hiphop est créé.");  
    println();

    // On stocke le chemin de toutes les images dans des listes séparées
    loadPicsPaths(TITRE, "PROJ09_HipHop/TITRE");
    loadPicsPaths(G1, "PROJ09_HipHop/G1");
    loadPicsPaths(G2, "PROJ09_HipHop/G2");
    loadPicsPaths(G3, "PROJ09_HipHop/G3");

    // Puis on génére les images 
    generatePicNumbers();
    theImages = new PImage[FinalSelection.size()];

    // Quelles images ont été choisies?
    println();
    println("Les images d'intro suivantes ont été choisies:");
    for (int i=0; i<FinalSelection.size(); i++)
    {
      println( FinalSelection.get(i));
      // On les charge:
      theImages[i] = loadImage(FinalSelection.get(i));
    }
    
    // Calcul de la hauteur 
    // Titre
    heightImages +=  theImages[0].height;
    heightImages += blank;
    heightImages += nbreLignes*theImages[1].height;
    heightImages += blank;
    heightImages += 4*theImages[1].height;
    
    
    //Taille de l'image 
    println();
    println("Largeur : 384 px // Hauteur: "+ heightImages+" px");

    // OK! on connait les images, la hauteur totale, la largeur, on peut créer l'image entière !
    createPic();
  }

  private void createPic()
  {
    int indexY = 0;
    pg = createGraphics(384, heightImages);
    pg.beginDraw();
    pg.background(255);
    
    // Titre
    pg.image(theImages[0], 0,0);
    indexY += theImages[0].height + blank;
    
    // Visuels
    // On boucle 16 fois
    for (int i =0; i<nbreLignes; i++)
    {
      pg.image(theImages[(i*3)+1], 0,indexY);
      pg.image(theImages[(i*3)+2], 128,indexY);
      pg.image(theImages[(i*3)+3], 256,indexY);
      indexY+=80;
    }
    
    indexY+=blank;
    // On boucle 4 fois
    for (int i =0; i<4; i++)
    {
      pg.image(theImages[theImages.length-1], 0,indexY);
      pg.image(theImages[theImages.length-1], 128,indexY);
      pg.image(theImages[theImages.length-1], 256,indexY);
      indexY+=80;
    }
    
    
    pg.endDraw();
  }

  public PGraphics getImage()
  {   
    return pg;
  }

  private void generatePicNumbers()
  {
    //Première partie
    FinalSelection.append(TITRE.get(0));


    // UNE LIGNE * 16
    for (int i = 0; i<nbreLignes; i++)
    {
    //Deuxième partie, tirage de 3 chiffres dans un tableau
    int[] myLyric = {(int)random(0, G1.size()), (int)random(0, G2.size()), (int)random(0, G3.size())};
    
    FinalSelection.append(G1.get(myLyric[0]));
    FinalSelection.append(G2.get(myLyric[1]));
    FinalSelection.append(G3.get(myLyric[2]));
    }
  
    // couplet
    int[] couplet = {(int)random(0, G1.size()), (int)random(0, G2.size()), (int)random(0, G3.size())};
    int randomGroup = (int)random(3);
    StringList[] coupletGroup = {G1, G2, G3};
    FinalSelection.append(coupletGroup[randomGroup].get(couplet[randomGroup]));
    
  }






  private void loadPicsPaths(StringList myStringList, String myDir)
  {
    println ("Je charge les images du dossier ../BTS_ThermalPrinting/data/"+ myDir);
    File dir = new File(sketchPath(""), "../BTS_ThermalPrinting/data/"+ myDir);
    if (dir.isDirectory()) {
      String[] contents = dir.list();
      for (int i = 0; i < contents.length; i++) {
        // skip hidden files and folders starting with a dot, load .png files only
        if (contents[i].charAt(0) == '.') continue;
        else if (contents[i].toLowerCase().endsWith(".png") || contents[i].toLowerCase().endsWith(".jpg") ) {
          File childFile = new File(dir, contents[i]);
          myStringList.append(childFile.getPath());
          println("---> "+contents[i]);
        }
      }
    }
    println();
  }
}