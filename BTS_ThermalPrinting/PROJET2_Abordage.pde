// Trame ??? Faut-il tramer ou non? 

class PROJET2_Abordage {
  StringList TITRE = new StringList();
  StringList PICS = new StringList();
  StringList END = new StringList();

  // Possibilité de modif
  int minPics = 5;
  int maxPics = 15;

  int nberOfPics;

  StringList FinalSelection = new StringList();

  PImage[] theImages;
  int heightImages;

  PGraphics pg;


  PROJET2_Abordage()
  {
    println("PROJET2_Abordage est créé");  
    println();
    // On stocke le chemin de toutes les images dans une liste
    loadPicsPaths(TITRE, "PROJ02_Abordage/TITRE");
    loadPicsPaths(PICS, "PROJ02_Abordage/PICS");
    loadPicsPaths(END, "PROJ02_Abordage/END");

    nberOfPics = (int)random(minPics, maxPics);

    // Puis on génére l'image en elle-même
    generatePicNumbers();

    theImages = new PImage[FinalSelection.size()];

    // Quelles images ont été choisies?
    println();
    println("Les images suivantes ont été choisies:");
    for (int i=0; i<FinalSelection.size(); i++)
    {
      println( FinalSelection.get(i));
      // On les charge:
      theImages[i] = loadImage(FinalSelection.get(i));
      heightImages += theImages[i].height;
    }

    //Taille de l'image
    println();
    println("Largeur :"+theImages[0].width+ " px // Hauteur: "+ heightImages+" px");

    // OK! on connait les images, la hauteur totale, la largeur, on peut créer l'image entière !
    createPic();
  }

  private void createPic()
  {
    int indexY = 0;
    pg = createGraphics(384, heightImages);
    pg.beginDraw();
    pg.background(255);
    for (int i=0; i<theImages.length; i++)
    {
      pg.image(theImages[i], 0, indexY);
      indexY += theImages[i].height;
    }
    pg.endDraw();
  }

  public PGraphics getImage()
  {   
    return pg;
  }

  private void generatePicNumbers()
  {
    //Première partie
    FinalSelection.append(TITRE.get((int)random(TITRE.size())));

    //Deuxième partie
    int actualNberOfPics = 0;
    int prevPic = 0;
    while (actualNberOfPics<nberOfPics)
    {
      int whatPic = (int)random(PICS.size());
      if (actualNberOfPics == 0)
      {
        FinalSelection.append(PICS.get(whatPic));
        prevPic = whatPic;
        actualNberOfPics++;
      } else {
        if (whatPic != prevPic)
        {
          FinalSelection.append(PICS.get(whatPic));
          prevPic = whatPic;
          actualNberOfPics++;
        }
      }
    }

    //Troisième partie
    FinalSelection.append(END.get((int)random(END.size())));
  }

  private void loadPicsPaths(StringList myStringList, String myDir)
  {
    println ("Je charge les images du dossier ../BTS_ThermalPrinting/data/"+ myDir);
    File dir = new File(sketchPath(""), "../BTS_ThermalPrinting/data/"+ myDir);
    if (dir.isDirectory()) {
      String[] contents = dir.list();
      for (int i = 0; i < contents.length; i++) {
        // skip hidden files and folders starting with a dot, load .png files only
        if (contents[i].charAt(0) == '.') continue;
        else if (contents[i].toLowerCase().endsWith(".png")) {
          File childFile = new File(dir, contents[i]);
          myStringList.append(childFile.getPath());
          println("---> "+contents[i]);
        }
      }
    }
    println();
  }
}