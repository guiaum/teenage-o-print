import java.util.Collections;

// Pas de trames !!

class PROJET4_Trame {

  StringList TRAMES = new StringList();
  StringList CADRE = new StringList();

  StringList FinalSelection = new StringList();
  
  PImage cadre;
  PGraphics pg;
  
  PFont f;

  PROJET4_Trame()
  {
    println("PROJET4_Trame est créé");  
    println();
    // On stocke le chemin de toutes les images dans des listes séparées
    loadPicsPaths(TRAMES, "PROJ04_Trame/TRAMES");
    loadPicsPaths(CADRE, "PROJ04_Trame/CADRE");
    
    cadre = loadImage(CADRE.get(0));
    
    //Taille de l'image
    println();
    println("Largeur : 384 px // Hauteur: "+ cadre.height+" px");
    
    //On créé la typo pour écrire le texte
    f = createFont("PROJ04_Trame/Helvetica-24.vlw", 24);
    textFont(f);
    
    // OK! on connait les images, la hauteur totale, la largeur, on peut créer l'image entière !
    createPic();
  }
  
  private void createPic()
  {
    pg = createGraphics(384, cadre.height);
    pg.beginDraw();
    pg.background(255);
    pg.image(cadre, 0,0);
    
    //On génére la trame qui fera l'oeuvre
    //1 - On mélange a liste des trames
    TRAMES.shuffle();
    PImage trame1 = loadImage(TRAMES.get(0));
    PImage trame2 = loadImage(TRAMES.get(1));
    
    //2- On dessine la première trame
    pg.image(trame1, 66,175);
    
    //3- Puis la deuxième, en produit
    pg.blendMode(MULTIPLY);
    pg.image(trame2, 66,175);
    
    String theDate = day()+"."+month()+"."+year()+" "+hour()+":"+minute()+":"+second();
    pg.fill(0);
    pg.textAlign(RIGHT);
    pg.text(theDate, 343,503);
    
    pg.endDraw();
  }
  
  public PGraphics getImage()
  {   
    return pg;
  }
  
  private void loadPicsPaths(StringList myStringList, String myDir)
  {
    println ("Je charge les images du dossier ../BTS_ThermalPrinting/data/"+ myDir);
    File dir = new File(sketchPath(""), "../BTS_ThermalPrinting/data/"+ myDir);
    if (dir.isDirectory()) {
      String[] contents = dir.list();
      for (int i = 0; i < contents.length; i++) {
        // skip hidden files and folders starting with a dot, load .png files only
        if (contents[i].charAt(0) == '.') continue;
        else if (contents[i].toLowerCase().endsWith(".png")) {
          File childFile = new File(dir, contents[i]);
          myStringList.append(childFile.getPath());
          println("---> "+contents[i]);
        }
      }
    }
    println();
  }
}